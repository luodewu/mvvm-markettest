# MVVM markettest

www.tigersgate.cn   基于Arouter的market的demo

## UI界面

<img src="https://gitlab.com/luodewu/mvvm-markettest/-/raw/main/Screenshot_2022-01-12-02-39-24-74_e9cc87c9ec1171fc35a53168e219c31a.jpg" width="300" align=center /> <img src="https://gitlab.com/luodewu/mvvm-markettest/-/raw/main/Screenshot_2022-01-12-02-39-27-33_e9cc87c9ec1171fc35a53168e219c31a.jpg" width="300" align=center />
<img src="https://gitlab.com/luodewu/mvvm-markettest/-/raw/main/Screenshot_2022-01-12-02-39-29-74_e9cc87c9ec1171fc35a53168e219c31a.jpg" width="300" align=center />
<br/>
<br/>


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:4f81cea1421c8924ea26effba2694a12?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:4f81cea1421c8924ea26effba2694a12?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:4f81cea1421c8924ea26effba2694a12?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/luodewu/mvvm-markettest.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:4f81cea1421c8924ea26effba2694a12?https://gitlab.com/luodewu/mvvm-markettest/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:4f81cea1421c8924ea26effba2694a12?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:4f81cea1421c8924ea26effba2694a12?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:4f81cea1421c8924ea26effba2694a12?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:4f81cea1421c8924ea26effba2694a12?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:4f81cea1421c8924ea26effba2694a12?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:4f81cea1421c8924ea26effba2694a12?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:4f81cea1421c8924ea26effba2694a12?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:4f81cea1421c8924ea26effba2694a12?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:4f81cea1421c8924ea26effba2694a12?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:4f81cea1421c8924ea26effba2694a12?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
